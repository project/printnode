<?php

namespace Drupal\printnode\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Implements PrintNode Profile form.
 *
 * @property \Drupal\printnode\Entity\PrintNodeProfileInterface $entity
 */
class PrintNodeProfileForm extends EntityForm {

  /**
   * The PrintNode manager.
   *
   * @var \Drupal\printnode\Service\PrintNodeManager
   */
  protected $printNodeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->printNodeManager = $container->get('printnode.manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {

    $form = parent::form($form, $form_state);

    $status = $this->entity->status();
    $api_key = $this->entity->getApiKey();

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $this->entity->label(),
      '#description' => $this->t('Label for the PrintNode Profile.'),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $this->entity->id(),
      '#machine_name' => [
        'exists' => '\Drupal\printnode\Entity\PrintNodeProfile::load',
      ],
      '#disabled' => !$this->entity->isNew(),
    ];

    $form['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#default_value' => $status,
    ];

    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#default_value' => $this->entity->get('description'),
      '#description' => $this->t('Description of the PrintNode Profile.'),
    ];

    $form['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Key'),
      '#default_value' => $api_key,
      '#description' => $this->t('The API Key'),
      '#required' => TRUE,
    ];

    $printer_options = [];
    if ($status && $api_key) {
      try {
        foreach ($this->printNodeManager->getPrinters($api_key) as $printer_id => $printer) {
          $printer_options[$printer_id] = $printer->name;
        }
      }
      catch (\Throwable $e) {
        $this->messenger()->addError($e->getMessage());
      }
    }
    $form['printer_id'] = [
      '#type' => 'select',
      '#title' => $this->t('Printer ID'),
      '#options' => $printer_options,
      '#default_value' => $this->entity->get('printer_id'),
      '#description' => $this->t('The Printer ID'),
      '#empty_option' => $this->t('- Not selected -'),
    ];

    $form['is_default'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('This is the default PrintNode Profile.'),
      '#default_value' => $this->entity->get('is_default'),
      '#description' => $this->t('This PrintNode Profile will be used for printing unless a contributed module or custom code decides otherwise.'),
      '#disabled' => $this->entity->isDefault(),
    ];
    if (!$this->entity->isDefault()) {
      /** @var \Drupal\printnode\PrintNodeStorageInterface $storage */
      $storage = $this->entityTypeManager->getStorage('printnode_profile');
      $default_printnode_profile = $storage->loadDefault();
      if (!$default_printnode_profile || $default_printnode_profile->id() == $this->entity->id()) {
        $form['is_default']['#default_value'] = TRUE;
        $form['is_default']['#title'] = $this->t('This is the default PrintNode Profile.');
        $form['is_default']['#disabled'] = TRUE;
      }
      else {
        $form['is_default']['#title'] = $this->t('Make this the default PrintNode Profile.');
      }
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $result = parent::save($form, $form_state);
    $message_args = ['%label' => $this->entity->label()];
    $message = $result == SAVED_NEW
      ? $this->t('Created new PrintNode Profile %label.', $message_args)
      : $this->t('Updated PrintNode Profile %label.', $message_args);
    $this->messenger()->addStatus($message);
    $form_state->setRedirectUrl($this->entity->toUrl('collection'));
    return $result;
  }

}
