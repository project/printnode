<?php

namespace Drupal\printnode;

use Drupal\Core\Config\Entity\ConfigEntityStorage;

/**
 * Defines the PrintNode Profile storage.
 */
class PrintNodeStorage extends ConfigEntityStorage implements PrintNodeStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function loadDefault() {
    $query = $this->getQuery();
    $query
      ->condition('is_default', TRUE)
      ->condition('status', 1)
      ->sort('id')
      ->range(0, 1)
      ->accessCheck(FALSE);
    $result = $query->execute();

    return $result ? $this->load(reset($result)) : NULL;
  }

}
