<?php

namespace Drupal\printnode;

use Drupal\Core\Config\Entity\ConfigEntityStorageInterface;

/**
 * Defines the interface for PrintNode Profile storage.
 */
interface PrintNodeStorageInterface extends ConfigEntityStorageInterface {

  /**
   * Loads the default PrintNode Profile.
   *
   * @return \Drupal\printnode\Entity\PrintNodeProfileInterface|null
   *   The default PrintNode Profile, if known.
   */
  public function loadDefault();

}
