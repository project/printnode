<?php

namespace Drupal\printnode\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining a printnode_profile entity type.
 */
interface PrintNodeProfileInterface extends ConfigEntityInterface {

  /**
   * Gets PrintNode Profile Api Key.
   *
   * @return string
   *   PrintNode Profile Api Key.
   */
  public function getApiKey();

  /**
   * Sets PrintNode Profile Api Key.
   *
   * @param string $api_key
   *   PrintNode Profile Api Key.
   *
   * @return $this
   */
  public function setApiKey(string $api_key);

  /**
   * Gets PrintNode Profile Printer ID.
   *
   * @return string
   *   PrintNode Profile Printer ID.
   */
  public function getPrinterId();

  /**
   * Sets PrintNode Profile Printer ID.
   *
   * @param int $printer_id
   *   PrintNode Profile Printer ID.
   *
   * @return $this
   */
  public function setPrinterId(int $printer_id);

  /**
   * Gets whether this is the default PrintNode Profile.
   *
   * @return bool
   *   TRUE if this is the default PrintNode Profile, FALSE otherwise.
   */
  public function isDefault();

  /**
   * Sets whether this is the default PrintNode Profile.
   *
   * @param bool $is_default
   *   Whether this is the default PrintNode Profile.
   *
   * @return $this
   */
  public function setDefault($is_default);

}
