<?php

namespace Drupal\printnode\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;

/**
 * Defines the PrintNode Profile entity type.
 *
 * @ConfigEntityType(
 *   id = "printnode_profile",
 *   label = @Translation("PrintNode Profile"),
 *   label_collection = @Translation("PrintNode Profiles"),
 *   label_singular = @Translation("PrintNode Profile"),
 *   label_plural = @Translation("PrintNode Profiles"),
 *   label_count = @PluralTranslation(
 *     singular = "@count PrintNode Profile",
 *     plural = "@count PrintNode Profiles",
 *   ),
 *   handlers = {
 *     "storage" = "Drupal\printnode\PrintNodeStorage",
 *     "list_builder" = "Drupal\printnode\PrintNodeProfileListBuilder",
 *     "form" = {
 *       "add" = "Drupal\printnode\Form\PrintNodeProfileForm",
 *       "edit" = "Drupal\printnode\Form\PrintNodeProfileForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm"
 *     }
 *   },
 *   config_prefix = "printnode_profile",
 *   admin_permission = "administer printnode_profile",
 *   links = {
 *     "collection" = "/admin/structure/printnode-profile",
 *     "add-form" = "/admin/structure/printnode-profile/add",
 *     "edit-form" = "/admin/structure/printnode-profile/{printnode_profile}",
 *     "delete-form" = "/admin/structure/printnode-profile/{printnode_profile}/delete"
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *     "api_key",
 *     "printer_id",
 *     "is_default"
 *   }
 * )
 */
class PrintNodeProfile extends ConfigEntityBase implements PrintNodeProfileInterface {

  /**
   * The PrintNode Profile ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The PrintNode Profile label.
   *
   * @var string
   */
  protected $label;

  /**
   * The PrintNode Profile status.
   *
   * @var bool
   */
  protected $status;

  /**
   * The PrintNode Profile description.
   *
   * @var string
   */
  protected $description;

  /**
   * The PrintNode Profile Api Key.
   *
   * @var string
   */
  protected $api_key;

  /**
   * The PrintNode Profile Printer ID.
   *
   * @var int
   */
  protected $printer_id;

  /**
   * Whether this is the default PrintNode Profile.
   *
   * @var bool
   */
  protected $is_default;

  /**
   * {@inheritdoc}
   */
  public function getApiKey() {
    return $this->get('api_key');
  }

  /**
   * {@inheritdoc}
   */
  public function setApiKey(string $api_key) {
    $this->set('api_key', $api_key);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getPrinterId() {
    return $this->get('printer_id');
  }

  /**
   * {@inheritdoc}
   */
  public function setPrinterId(int $printer_id) {
    $this->set('printer_id', $printer_id);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isDefault() {
    return (bool) $this->get('is_default');
  }

  /**
   * {@inheritdoc}
   */
  public function setDefault($is_default) {
    $this->set('is_default', (bool) $is_default);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE) {
    parent::postSave($storage, $update);

    $default = $this->isDefault();
    $original_default = isset($this->original) ? $this->original->isDefault() : FALSE;
    if ($default && !$original_default) {
      // The PrintNode Profile was set as default, remove the flag from other
      // PrintNode Profiles.
      $profile_ids = $storage->getQuery()
        ->condition('id', $this->id(), '<>')
        ->condition('is_default', TRUE)
        ->accessCheck(FALSE)
        ->execute();
      foreach ($profile_ids as $profile_id) {
        /** @var \Drupal\printnode\Entity\PrintNodeProfileInterface $profile */
        $profile = $storage->load($profile_id);
        $profile->setDefault(FALSE);
        $profile->save();
      }
    }
  }

}
