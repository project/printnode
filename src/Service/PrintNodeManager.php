<?php

namespace Drupal\printnode\Service;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\printnode\Entity\PrintNodeProfileInterface;
use PrintNode\Client;
use PrintNode\Credentials\ApiKey;
use PrintNode\Entity\PrintJob;
use Psr\Log\LoggerInterface;

/**
 * Implements PrintNode manager.
 */
class PrintNodeManager {

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The PrintNode Profile storage.
   *
   * @var \Drupal\printnode\PrintNodeStorageInterface
   */
  protected $printNodeStorage;

  /**
   * Constructs a new PrintNodeManager object.
   *
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(LoggerInterface $logger, EntityTypeManagerInterface $entity_type_manager) {
    $this->logger = $logger;
    $this->entityTypeManager = $entity_type_manager;
    $this->printNodeStorage = $entity_type_manager->getStorage('printnode_profile');
  }

  /**
   * Prepare PriJob for printing.
   *
   * @param \PrintNode\Entity\PrintJob $printJob
   *   PrintJob entity.
   *
   * @return false|string
   *   Prepared data for printing.
   */
  private function jsonSerialize(PrintJob $printJob) {
    $data = [
      'printer' => $printJob->printer,
      'contentType' => $printJob->contentType,
      'content' => $printJob->content,
      'title' => $printJob->title,
      'source' => $printJob->source,
    ];
    $data = array_filter($data);
    return Json::encode($data);
  }

  /**
   * Get PrintNode client.
   *
   * @param string $api_key
   *   Api Key.
   *
   * @return \PrintNode\Client
   *   The client.
   */
  public function getClient(string $api_key) {
    $credentials = new ApiKey($api_key);
    return new Client($credentials);
  }

  /**
   * Loads the default PrintNode Profile.
   *
   * @return \Drupal\printnode\Entity\PrintNodeProfileInterface|null
   *   The default PrintNode Profile, if known.
   */
  public function getDefaultProfile() {
    return $this->printNodeStorage->loadDefault();
  }

  /**
   * Gets the printers list.
   *
   * @param string $api_key
   *   Api Key.
   *
   * @return \PrintNode\Entity\Printer[]
   *   Printers list.
   */
  public function getPrinters(string $api_key) {
    $client = $this->getClient($api_key);
    return $client->viewPrinters();
  }

  /**
   * Print PDF file.
   *
   * @param string $file_path
   *   PDF file path.
   * @param \Drupal\printnode\Entity\PrintNodeProfileInterface $profile
   *   PrintNode Profile.
   * @param array $params
   *   Additional print params "title" and "description" (optional).
   *
   * @return int
   *   Print Job ID.
   */
  public function printPdfFile(string $file_path, PrintNodeProfileInterface $profile, array $params = []) {
    $client = $this->getClient($profile->getApiKey());
    $printJob = new PrintJob($client);
    if (!empty($params['title'])) {
      $printJob->title = $params['title'];
    }
    if (!empty($params['description'])) {
      $printJob->source = $params['description'];
    }
    $printJob->printer = $profile->getPrinterId();
    $printJob->addPdfFile($file_path);
    $response = $client->makeRequest('printjobs', 'POST', $this->jsonSerialize($printJob));
    return $response->body;
  }

}
